## Name
Nokia 3310
It's just a product name but because of the task I have to put here at least one hundred characters.

## Description
The  **Nokia 3310**  is a  [GSM](https://en.wikipedia.org/wiki/GSM "GSM")  mobile phone announced on 1 September 2000,  and released in the fourth quarter of the year, replacing the popular  [Nokia 3210](https://en.wikipedia.org/wiki/Nokia_3210). It sold very well,  [being one of the most successful phones](https://en.wikipedia.org/wiki/List_of_best-selling_mobile_phones "Nokia")  with 126 million units sold worldwide,  and being one of  [Nokia](https://en.wikipedia.org/wiki/Nokia)'s most iconic devices. The phone is still widely acclaimed and has gained a  [cult status](https://en.wikipedia.org/wiki/Cult_status "Cult status")  due to its near indestructibility.

## Specifications
* **Operating System**: Series 20
* **CPU**: [Texas Instruments](https://en.wikipedia.org/wiki/Texas_Instruments) MAD2WD1
* **Screen resolution**: 82 x 48 pixel
* **Memory**: Built-in 1 KB
* **Battery**: Removable  BMC-3 (NiMH) 900 mAh, or  BLC-2 (Li-ion) 1000 mAh
* **Form factor**: Candy bar
* **Length**: 113 mm
* **Width**: 48 mm
* **Thickness**: 22 mm
* **Weight**: 133 g

## Photos
|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Nokia_3310_Blue_R7309170_%28retouch%29.png/800px-Nokia_3310_Blue_R7309170_%28retouch%29.png" height="200">|
|:--:|
|*Blue version of the Nokia 3310 mobile phone with German menu.*|

|<img src="https://upload.wikimedia.org/wikipedia/commons/3/32/Nokia3310_Sahara_Gelb.jpg" height="200">|
|:--:|
|*Nokia 3310 with an official "Sahara Yellow" colour Xpress-on cover.*|

|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Nokia_3310_grey_all_sides.jpg/1024px-Nokia_3310_grey_all_sides.jpg" height="200">|
|:--:|
|*A grey, Orange-branded 3310 seen from multiple angles.*|

|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/Nokia_3310_grey_inside_and_back_panel.jpg/1024px-Nokia_3310_grey_inside_and_back_panel.jpg" height="200">|
|:--:|
|*Inside a Nokia 3310 with the battery removed.*|

## Similar devices
* Nokia 3210
* Nokia 3310 (2017)
* Nokia 3315
* Nokia 3320
* Nokia 3330
* Nokia 3350
* Nokia 3360
* Nokia 3361
* Nokia 3390
* Nokia 3395
